//
//  DetailViewController.h
//  Recipes
//
//  Created by Tyler Barnes on 2/8/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>{
    
    //Assigning Arrays to be accessed for data input
    NSArray* RecipeArray;
    NSArray* ingredients;
    
    //Setting up Global Rect to resize TableView of Ingredients
    CGRect TableViewBounds;
}

@property (nonatomic, strong) NSDictionary* RecipeDictionary;

@property(nonatomic) BOOL isSomethingEnabled;

//Declaring URL action
-(void) openLink;

@end
