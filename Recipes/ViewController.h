//
//  ViewController.h
//  Recipes
//
//  Created by Tyler Barnes on 2/8/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewController.h"

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>{
    
    UITableView *RecipeTable;
    
    NSArray* DataArray;
}


@end

