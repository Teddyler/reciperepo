//
//  DetailViewController.m
//  Recipes
//
//  Created by Tyler Barnes on 2/8/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import "DetailViewController.h"
#import "ViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Establishing Readable Background Color
    self.view.backgroundColor = [UIColor whiteColor];
    
    //Reading in info from plist
    NSString* path = [[NSBundle mainBundle] pathForResource:@"RecipePropertyList" ofType:@"plist"];
    RecipeArray = [[NSArray alloc] initWithContentsOfFile: path];
    
    //Creating Master ScrollView
    UIScrollView * scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:scrollView];

    //Title pulled from plist
    UITextView* title = [[UITextView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 150)];
    title.text = self.RecipeDictionary[@"Title"];
    title.font = [UIFont systemFontOfSize:50];
    title.textAlignment = NSTextAlignmentCenter;
    [scrollView addSubview:title];
    
    //Disable Scrolling and Editing
    title.scrollEnabled = NO;
    title.editable = NO;
    
    //Updating TextView height while maintaining width
    [title sizeToFit];
    CGRect ViewFrame = title.frame;
    ViewFrame.size.width = self.view.frame.size.width;
    title.frame = ViewFrame;
    
    
    //Image pulled from plist
    UIImageView* img = [[UIImageView alloc]initWithFrame:CGRectMake(0, title.frame.origin.y + title.frame.size.height, self.view.frame.size.width, self.view.frame.size.height/3)];
    img.image = [UIImage imageNamed:self.RecipeDictionary[@"Image"]];
    img.contentMode = UIViewContentModeScaleAspectFit;
    [scrollView addSubview:img];
    
    //List of Ingredients pulled from an Array in plist
    UITableView * ingredientsTable = [[UITableView alloc]initWithFrame:CGRectMake(0, img.frame.origin.y + img.frame.size.height, self.view.frame.size.width - 20, 250) style:UITableViewStylePlain];
    ingredientsTable.delegate = self;
    ingredientsTable.dataSource = self;
    [scrollView addSubview:ingredientsTable];
    
    //Disable Scrolling
    ingredientsTable.scrollEnabled = NO;
    
    //Resizing TableView to show all ingredients
    TableViewBounds = [ingredientsTable rectForSection:0];
    ViewFrame = ingredientsTable.frame;
    ViewFrame.size.height = TableViewBounds.size.height;
    ingredientsTable.frame = ViewFrame;
    
    //Process directions pulled from plist
    UITextView* process = [[UITextView alloc]initWithFrame:CGRectMake(5, ingredientsTable.frame.origin.y + ingredientsTable.frame.size.height, self.view.frame.size.width - 10, self.view.frame.size.height)];
    process.text = self.RecipeDictionary[@"Process"];
    process.textAlignment = NSTextAlignmentLeft;
    process.contentInset = UIEdgeInsetsMake(0, 5, 0, 20);

    [scrollView addSubview:process];
    
    //Disable Scrolling and Editing
    process.scrollEnabled = NO;
    process.editable = NO;
    
    //Update Height of TextView
    [process sizeToFit];
    
    //Adding Link to recipe the launches in Safari
    UIButton* WebLink = [[UIButton alloc]initWithFrame:CGRectMake(0, process.frame.origin.y + process.frame.size.height, self.view.frame.size.width, 0)];
    [WebLink setTitle:self.RecipeDictionary[@"Web Link"] forState:UIControlStateNormal];
    [WebLink setTitleColor: [UIColor blueColor] forState:UIControlStateNormal];
    [WebLink setTitleColor: [UIColor purpleColor] forState:UIControlStateHighlighted];
    [WebLink addTarget:self action: @selector(openLink) forControlEvents:UIControlEventTouchUpInside];
    WebLink.titleLabel.lineBreakMode = NSLineBreakByCharWrapping;
    WebLink.contentHorizontalAlignment = NSTextAlignmentLeft;
    WebLink.titleLabel.font = [UIFont systemFontOfSize:11];
    WebLink.contentEdgeInsets = UIEdgeInsetsMake(5, 15, 10, 25);
    [scrollView addSubview:WebLink];

//    //Update Height of TextView
    [WebLink sizeToFit];
    ViewFrame = WebLink.frame;
    ViewFrame.size.width = self.view.frame.size.width;
    WebLink.frame = ViewFrame;
    
    //Resizing ScrollView to fit contained views
    CGRect contentRect = CGRectZero;
    for (UIView *view in scrollView.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    [scrollView setContentSize: contentRect.size];
    
}

//Getting number of items in ingredients array
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.RecipeDictionary[@"Ingredients"] count];
}

//Populating ingredients array
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //Creating new cell for item in Ingredients List
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines = 0;
    }
    
    //Setting text for cell
    ingredients = self.RecipeDictionary[@"Ingredients"];
    NSString* stringToDisplay = ingredients[indexPath.row];
    cell.textLabel.text = stringToDisplay;
    
    //Resizing cell to fit text
    [tableView sizeToFit];
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) openLink {

    //Fires when URL is clicked to redirect to recipe in Safari
    NSDictionary *options = @{UIApplicationOpenURLOptionUniversalLinksOnly : @NO};
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: self.RecipeDictionary[@"Web Link"]] options:options completionHandler:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
