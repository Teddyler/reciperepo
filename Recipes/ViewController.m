//
//  ViewController.m
//  Recipes
//
//  Created by Tyler Barnes on 2/8/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //Creating path to plist
    NSString* path = [[NSBundle mainBundle] pathForResource:@"RecipePropertyList" ofType:@"plist"];
    DataArray = [[NSArray alloc] initWithContentsOfFile: path];
    
    //Creating TableView for main menu
    RecipeTable = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    RecipeTable.delegate = self;
    RecipeTable.dataSource = self;
    [self.view addSubview: RecipeTable];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return DataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //Creating cells for main menu
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    //Populating cell labels
    NSDictionary* Recipes = DataArray[indexPath.row];
    NSString* stringToDisplay = Recipes[@"Title"];
    cell.textLabel.text = stringToDisplay;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary* Recipes = DataArray[indexPath.row];
    
    DetailViewController* dvc = [DetailViewController new];
    
    dvc.RecipeDictionary = Recipes;
    
    [self.navigationController pushViewController: dvc animated:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"showDetailSegue"]){
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        DetailViewController *controller = (DetailViewController *)navController.topViewController;
        controller.isSomethingEnabled = YES;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
